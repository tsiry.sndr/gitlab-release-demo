import Client, { connect } from "../../deps.ts";

export enum Job {
  build = "build",
  releaseUpload = "release_upload",
  releaseCreate = "release_create",
}

export const exclude = [];

export const build = async (src = ".") => {
  await connect(async (client: Client) => {
    const context = client.host().directory(src);
    const ctr = client
      .pipeline(Job.build)
      .container()
      .from("rust:latest")
      .withDirectory("/app", context, { exclude })
      .withWorkdir("/app")
      .withMountedCache("/app/target", client.cacheVolume("target"))
      .withMountedCache("/root/cargo/registry", client.cacheVolume("registry"))
      .withMountedCache("/assets", client.cacheVolume("gl-release-assets"))
      .withExec([
        "cargo",
        "build",
        "--release",
        "--target",
        "x86_64-unknown-linux-gnu",
      ])
      .withExec([
        "tar",
        "czvf",
        `/assets/hello_${
          Deno.env.get("TAG") || ""
        }_x86_64-unknown-linux-gnu.tar.gz`,
        "target/x86_64-unknown-linux-gnu/release/gitlab-release-demo",
      ])
      .withExec([
        "sh",
        "-c",
        `shasum -a 256 /assets/hello_${
          Deno.env.get("TAG") || ""
        }_x86_64-unknown-linux-gnu.tar.gz > /assets/hello_${
          Deno.env.get("TAG") || ""
        }_x86_64-unknown-linux-gnu.tar.gz.sha256`,
      ]);

    await ctr.stdout();
  });
  return "Done";
};

export const releaseCreate = async (
  src = ".",
  token?: string,
  tag?: string
) => {
  await connect(async (client: Client) => {
    const TAG = Deno.env.get("TAG") || tag || "latest";
    const context = client.host().directory(src);
    const ctr = client
      .pipeline(Job.releaseCreate)
      .container()
      .from("pkgxdev/pkgx:latest")
      .withExec(["apt-get", "update"])
      .withExec(["apt-get", "install", "-y", "ca-certificates"])
      .withExec(["pkgx", "install", "glab", "git"])
      .withMountedCache("/assets", client.cacheVolume("gl-release-assets"))
      .withDirectory("/app", context)
      .withWorkdir("/app")
      .withExec([
        "glab",
        "auth",
        "login",
        "--token",
        Deno.env.get("GITLAB_ACCESS_TOKEN") || token!,
      ])
      .withExec(["glab", "release", "create", TAG]);

    await ctr.stdout();
  });

  return "Done";
};

export const releaseUpload = async (
  src = ".",
  token?: string,
  tag?: string,
  file?: string
) => {
  await connect(async (client: Client) => {
    const TAG = Deno.env.get("TAG") || tag || "latest";
    const FILE = Deno.env.get("FILE") || file!;
    const context = client.host().directory(src);
    const ctr = client
      .pipeline(Job.releaseUpload)
      .container()
      .from("pkgxdev/pkgx:latest")
      .withExec(["apt-get", "update"])
      .withExec(["apt-get", "install", "-y", "ca-certificates"])
      .withExec(["pkgx", "install", "glab", "git"])
      .withMountedCache("/assets", client.cacheVolume("gl-release-assets"))
      .withDirectory("/app", context)
      .withWorkdir("/app")
      .withExec([
        "glab",
        "auth",
        "login",
        "--token",
        Deno.env.get("GITLAB_ACCESS_TOKEN") || token!,
      ])
      .withExec(["glab", "release", "upload", TAG, FILE]);

    await ctr.stdout();
  });

  return "Done";
};

export type JobExec = (
  src?: string,
  token?: string,
  tag?: string,
  file?: string
) => Promise<string>;

export const runnableJobs: Record<Job, JobExec> = {
  [Job.build]: build,
  [Job.releaseUpload]: releaseUpload,
  [Job.releaseCreate]: releaseCreate,
};

export const jobDescriptions: Record<Job, string> = {
  [Job.build]: "Compile the project",
  [Job.releaseUpload]: "Upload asset files to a Gitlab Release",
  [Job.releaseCreate]: "Create a Gitlab Release",
};
